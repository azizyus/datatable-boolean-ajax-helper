<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemActivitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("tableEnum");
            $table->boolean("value")->default(false);
            $table->unsignedInteger("purposeEnum");
            $table->unsignedInteger("modelId");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_activities');
    }
}
