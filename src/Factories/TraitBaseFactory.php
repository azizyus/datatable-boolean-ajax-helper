<?php


namespace Azizyus\DataTableBooleanAjax\Factories;


use Azizyus\DataTableBooleanAjax\Model\ItemActivity;

class TraitBaseFactory
{

    public static function makeRelation($model, $booleanPurpose,$primaryKey="id")
    {
        return $model->hasOne(config("datatable-ajax-perms.activityModel"),"modelId",$primaryKey)
            ->where("tableEnum",$model->tableEnum())
            ->where("purposeEnum",$booleanPurpose);
    }

}
