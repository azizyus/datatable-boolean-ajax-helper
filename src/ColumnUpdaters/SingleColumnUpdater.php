<?php


namespace  Azizyus\DataTableBooleanAjax\ColumnUpdaters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class SingleColumnUpdater
{

    protected $modelNameSpace;
    protected $column;
    protected $id;

    public function setRequest(Request $request)
    {
        $this->id = $request->get("id");
    }

    public function update()
    {
        $column = $this->column;
        $foundModel = $this->modelNameSpace::find($this->id);
        if($foundModel)
        {
            $foundModel->{$column} = !$foundModel->{$column};
            $foundModel->save();
            return true;
        }
        return false;
    }

    public function getCurrentBoolValue(Model $model)
    {
        return $model->{$this->column};
    }





}
