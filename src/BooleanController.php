<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 7.06.2019
 * Time: 01:30
 */

namespace Azizyus\DataTableBooleanAjax;


use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Azizyus\DataTableBooleanAjax\ActivityHelpers\Updater;

class BooleanController extends Controller
{

    public function update(Request $request)
    {
        $updaterClassNameSpace = $request->get("updaterClassNameSpace",null);



        if($updaterClassNameSpace == null)
        {
            $updater = new Updater($request);
            $updater->parse();
            if($updater->update())
                return ["message" => "Success"];
        }
        else
        {
            $updater = new $updaterClassNameSpace();
            $updater->setRequest($request);
            $updater->update();
            return ["message" => "Success"];
        }

        return ["message" => "couldnt update"];

    }

}
