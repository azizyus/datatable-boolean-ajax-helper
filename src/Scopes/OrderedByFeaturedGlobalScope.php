<?php


namespace Azizyus\DataTableBooleanAjax\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class OrderedByFeaturedGlobalScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->orderBy("isFeatured","DESC");
    }


}
