<?php


namespace Azizyus\DataTableBooleanAjax\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class IsActiveGlobalScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->with("activity");
        $builder->whereHas("activity",function ($query){

            $query->where("value",true);

        });
    }


}
