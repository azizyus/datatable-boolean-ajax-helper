<?php


namespace Azizyus\DataTableBooleanAjax\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class IsMainPageGlobalScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
       $builder->with("mainPage");
       $builder->whereHas("mainPage",function ($query){

           $query->where("value",true);

       });
    }


}
