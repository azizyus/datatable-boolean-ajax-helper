<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 7.06.2019
 * Time: 01:29
 */

namespace Azizyus\DataTableBooleanAjax;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class DataTableBooleanAjaxServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadViewsFrom(__DIR__,"DataTableBoolean");
        $this->mergeConfigFrom(__DIR__."/datatable-ajax-perms.php","datatable-ajax-perms");
        $this->loadMigrationsFrom(__DIR__."/Migrations");

        $this->registerRoutes();


        $this->publishes([

            __DIR__."/datatable-ajax-perms.php" => config_path("datatable-ajax-perms.php"),

        ],"azizyus/DataTableBooleanAjaxHelperConfig");


    }

    public function registerRoutes()
    {
       return Route::prefix("admin")
            ->namespace("Azizyus\DataTableBooleanAjax")
            ->group(__DIR__."/routes.php");
    }

}
