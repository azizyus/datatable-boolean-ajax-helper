<?php


namespace Azizyus\DataTableBooleanAjax\Traits;


use Azizyus\DataTableBooleanAjax\Events\CreateEventImplementation;
use Azizyus\DataTableBooleanAjax\Factories\TraitBaseFactory;
use Azizyus\DataTableBooleanAjax\ActivityHelpers\ItemActivityBooleanChecker;
use Azizyus\DataTableBooleanAjax\ActivityHelpers\Purposes;

trait HasIsActive
{

    public function activity()
    {
        return TraitBaseFactory::makeRelation($this,Purposes::_IS_ACTIVE);
    }

    public function isActive()
    {
        return ItemActivityBooleanChecker::check($this->activity);
    }

    protected static function bootHasIsActive()
    {
        static::created(CreateEventImplementation::getClosure(Purposes::_IS_ACTIVE,true));
    }

}
