<?php


namespace Azizyus\DataTableBooleanAjax\Traits;


use Azizyus\DataTableBooleanAjax\Events\CreateEventImplementation;
use Azizyus\DataTableBooleanAjax\Factories\TraitBaseFactory;
use Azizyus\DataTableBooleanAjax\ActivityHelpers\ItemActivityBooleanChecker;
use Azizyus\DataTableBooleanAjax\ActivityHelpers\Purposes;

trait HasIsMainPage
{

    public function mainPage()
    {
        return TraitBaseFactory::makeRelation($this,Purposes::_IN_MAIN_PAGE);
    }

    public function isMainPage()
    {
        return ItemActivityBooleanChecker::check($this->mainPage);
    }

    protected static function bootHasIsMainPage()
    {
        static::created(CreateEventImplementation::getClosure(Purposes::_IN_MAIN_PAGE,false));
    }

}
