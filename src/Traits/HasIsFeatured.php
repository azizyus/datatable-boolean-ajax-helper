<?php


namespace Azizyus\DataTableBooleanAjax\Traits;


use Azizyus\DataTableBooleanAjax\Events\CreateEventImplementation;
use Azizyus\DataTableBooleanAjax\Factories\TraitBaseFactory;
use Azizyus\DataTableBooleanAjax\ActivityHelpers\ItemActivityBooleanChecker;
use Azizyus\DataTableBooleanAjax\ActivityHelpers\Purposes;

trait HasIsFeatured
{

    public function featured()
    {
        return TraitBaseFactory::makeRelation($this,Purposes::_FEATURED);
    }

    public function isFeatured()
    {
        return ItemActivityBooleanChecker::check($this->featured);
    }

    protected static function bootHasIsFeatured()
    {
        static::created(CreateEventImplementation::getClosure(Purposes::_FEATURED,true));
    }

}
