<?php


namespace Azizyus\DataTableBooleanAjax\ActivityHelpers;


class ActivityModelFinder
{


    public function getModelNamespace()
    {
        return config("datatable-ajax-perms.activityModel");
    }

}