<?php


namespace Azizyus\DataTableBooleanAjax\ActivityHelpers;


use Azizyus\DataTableBooleanAjax\Model\ItemActivity;
use Illuminate\Http\Request;

class Updater
{


    public $request;
    public $tableEnum;
    public $id;
    public $purposeEnum;
    public $tableModelNamespace;
    public $relationMethod;
    public $activityModelFinder;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->activityModelFinder = new ActivityModelFinder();
    }

    public function parse()
    {
        $this->tableEnum = $this->request->get("tableEnum");
        $this->id = $this->request->get("id");
        $this->purposeEnum = $this->request->get("purposeEnum");
        $this->tableModelNamespace = $this->request->get("tableModelNameSpace");
        $this->relationMethod = $this->request->get("relationMethod");
    }

    public function hasPerm()
    {
        return in_array($this->tableEnum,config("datatable-ajax-perms.acceptedModels"));
    }

    public function update()
    {
        if($this->hasPerm())
        {


            $relationMethod = $this->relationMethod;
            $foundModel = $this->tableModelNamespace::with($relationMethod)->find($this->id);

            if($foundModel)
            {

                $activityModel = $this->activityModelFinder->getModelNamespace();

                $itemActivity = $activityModel::updateOrCreate([
                    "purposeEnum" => $this->purposeEnum,
                    "tableEnum" => $this->tableEnum,
                    "modelId" => $this->id
                ]);

                $itemActivity->value = !$itemActivity->value;

                $itemActivity->save();
                $foundModel->touch();
                //  $foundModel->updated_at=now();
                //  $foundModel->save();
                return true;
            }
        }
        return false;
    }

}
