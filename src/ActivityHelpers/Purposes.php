<?php


namespace Azizyus\DataTableBooleanAjax\ActivityHelpers;


class Purposes
{

    const _IS_ACTIVE = 1;
    const _IN_MAIN_PAGE = 2;
    const _FEATURED = 3;

}
