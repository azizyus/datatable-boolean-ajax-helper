<?php


namespace Azizyus\DataTableBooleanAjax\ActivityHelpers;


class ItemActivityBooleanChecker
{

    public static function check($itemActivity,$default=false)
    {
        if($itemActivity)
        {
            return $itemActivity->value;
        }
        return $default;
    }

}
