<?php


namespace Azizyus\DataTableBooleanAjax\HtmlBuilder;

use Illuminate\Database\Eloquent\Model;

class ActivityColumnBuilderForSingleColumn
{

    public static function makeBaseSingleColumn(Model $model, String $updaterNamespace)
    {
        $signature = "----";
        if((new $updaterNamespace)->getCurrentBoolValue($model)) $signature = "++++";

        return "<span style='cursor:pointer;' data-updater-namespace='$updaterNamespace' data-model-id='".$model->getKey()."'  class='boolean-datatable-value'>$signature</span>";
    }

}
