<?php


namespace Azizyus\DataTableBooleanAjax\HtmlBuilder\Interfaces;


interface ICanBuildColumn
{

    public function tableEnum() : Int;

}