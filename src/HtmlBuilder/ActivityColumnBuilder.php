<?php


namespace Azizyus\DataTableBooleanAjax\HtmlBuilder;

use Azizyus\DataTableBooleanAjax\HtmlBuilder\Interfaces\ICanBuildColumn;
use Illuminate\Database\Eloquent\Model;

class ActivityColumnBuilder
{

    protected function makeBase(ICanBuildColumn $model, $purposeEnum, $tableEnum, $method,$relationMethod)
    {
        $signature = "----";
        if($model->{$method}()) $signature = "++++";

        return "<span style='cursor:pointer;' data-relation-method='".$relationMethod."' data-table-model-namespace='".get_class($model)."' data-purpose='".$purposeEnum."' data-model-id='".$model->id."' data-table-enum='".$tableEnum."' class='boolean-datatable-value'>$signature</span>";
    }

    public function make(ICanBuildColumn $model,$purposeEnum, $method,$relationMethod)
    {
        return $this->makeBase($model,$purposeEnum,$model->tableEnum(),$method,$relationMethod);
    }


}
