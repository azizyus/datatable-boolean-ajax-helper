<script>


    $(document).ready(function () {

        $(document).on("click",".boolean-datatable-value",function () {


            var id        = $(this).attr("data-model-id");
            var tableEnum = $(this).attr("data-table-enum");
            var purpose    = $(this).attr("data-purpose");
            var relationMethod = $(this).attr("data-relation-method");
            var tableModelNameSpace = $(this).attr("data-table-model-namespace");
            var updaterClassNameSpace = $(this).attr("data-updater-namespace");

            console.log(id);
            console.log(tableEnum);
            console.log(purpose);

            var me = this;

            $.ajax({

                "url":"{{route("datatable.update")}}",
                "data" : {"tableModelNameSpace":tableModelNameSpace,"relationMethod":relationMethod,"updaterClassNameSpace":updaterClassNameSpace,"tableEnum":tableEnum, "id":id,"purposeEnum":purpose},
                "type":"GET",
                success:function (data) {

                    if($(me).html() == "++++") $(me).html("----");
                    else $(me).html("++++");

                },
                error:function () {

                    alert("An error occurred, report this to system admin");

                }

            });

        });

    });

</script>
