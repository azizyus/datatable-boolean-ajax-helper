<?php

namespace Azizyus\DataTableBooleanAjax\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $tableEnum
 * @property int $modelId
 * @property string $created_at
 * @property string $updated_at
 */
class ItemActivity extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['tableEnum','purposeEnum','value', 'modelId', 'created_at', 'updated_at'];

}
