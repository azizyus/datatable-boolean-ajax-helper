<?php


namespace Azizyus\DataTableBooleanAjax\Events;


use Azizyus\DataTableBooleanAjax\Model\ItemActivity;

class CreateEventImplementation
{

    public static function getClosure($purposeEnum,$defaultValue = true) : \Closure
    {

        return function($model) use ($purposeEnum,$defaultValue) {

            $mainModelNameSpace = config("datatable-ajax-perms.activityModel");

            $mainModelNameSpace::create([

                "tableEnum" => $model->tableEnum(),
                "purposeEnum" => $purposeEnum,
                "modelId" => $model->id,
                "value" => $defaultValue //which is defines default value when the model created

            ]);

        };

    }

}
